﻿using CsvHelper;
using EyeTrackingResultAnalyzer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace EyeTrackingResultAnalyzer
{
    public static class CsvFixationCalculator
    {
        public static List<EyeTrackerData> GetContent(string pathToFile)
        {
            using (var reader = new StreamReader(pathToFile))
            using(var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                return csv.GetRecords<EyeTrackerData>().ToList();
            }
        }

        public static FixationsData GetFixationsDetails(List<EyeTrackerData> fileData)
        {
            var titleElements = fileData.Select(x => x.DisplayName).Distinct().ToList();
            var elementPerTime = new Dictionary<string, int>();
            var numberOfTransitions = new List<int>();
            for (int j = 0; j < titleElements.Count; j++)
            {
                elementPerTime.Add(titleElements[j], 1);
                numberOfTransitions.Add(0);
                for (int i = 0; i < fileData.Count - 1; i++)
                {
                    //obliczanie ile użytkownik poświęcił czasu na patrzenie na dany element
                    if(IsFixation(titleElements[j], fileData[i].DisplayName, fileData[i+1].DisplayName))
                    {
                        elementPerTime[titleElements[j]] += fileData[i + 1].Time - fileData[i].Time;
                    }
                    else if(i > 0 && HasElementChanged(titleElements[j], fileData[i - 1].DisplayName, fileData[i].DisplayName, fileData[i + 1].DisplayName))
                    {
                        numberOfTransitions[j]++;
                    }
                    //else if(i>0 && IsSingleGlance(titleElements[j], fileData[i - 1].DisplayName, fileData[i].DisplayName, fileData[i + 1].DisplayName))
                    //{
                    //    elementPerTime[titleElements[j]] += 1;
                    //}
                }
            }
            return new FixationsData(elementPerTime, numberOfTransitions);
        }

        public static FixationsData GetTimeSpendOnModelElement(IList<EyeTrackerData> fileData)
        {           
            var fixationTime = 0;
            var indexOfSampleInOneFixation = 0;
            var fixation = new Fixation();
            var fixations = new List<Fixation>();

            for (int i = 1; i < fileData.Count - 1; i++)
            {
                string currentlyCheckedElement = fileData[i - 1].ElementName;
                if (IsFixation(currentlyCheckedElement, currentlyCheckedElement, fileData[i].ElementName))
                {                    
                    var diff = fileData[i].Time - fileData[i - 1].Time;
                    fixationTime += diff;
                    fixation.Start(indexOfSampleInOneFixation, fileData[i - 1].Time);
                    indexOfSampleInOneFixation++;
                }
                if (HasElementChanged(currentlyCheckedElement, currentlyCheckedElement, fileData[i].ElementName, fileData[i + 1].ElementName))
                {
                    if (WasFixationLongerThan150ms(fixationTime))
                    {                       
                        fixation.End(currentlyCheckedElement, fixationTime, fileData[i].Time);
                    }
                    else
                    {
                        fixation.StartTime = 0;
                    }
                    fixationTime = 0;
                    indexOfSampleInOneFixation = 0;    
                    if(fixation.StartTime != 0)
                        fixations.Add(fixation);
                    fixation = new Fixation();
                }
            }
            return new FixationsData(fixations);
        }

        private static Dictionary<string, int> FillWithModelElNamesAndStartTime(IList<EyeTrackerData> fileData)
        {
            var titleElements = fileData.Select(x => x.ElementName).Distinct().ToList();
            var elementPerTime = new Dictionary<string, int>();
            var initialFixationTime = 0;
            foreach(var item in titleElements)
            {
                elementPerTime.Add(item, initialFixationTime);
            }

            return elementPerTime;
        }

        private static bool WasFixationLongerThan150ms(int fixationTime)
        {
            return fixationTime >= 150;
        }

        private static bool IsFixation(string titleElem, string currentRow, string nextRow)
        {
            return currentRow == titleElem && nextRow == titleElem;
        }

        private static bool HasElementChanged(string titleElem, string prevRow, string currentRow, string nextRow)
        {
            return prevRow == titleElem && currentRow == titleElem && nextRow != titleElem;
        }

        private static bool IsSingleGlance(string titleElem, string prevRow, string currentRow, string nextRow)
        {
            return currentRow == titleElem && prevRow != titleElem && nextRow != titleElem;
        }
    }
}
