﻿using System;

namespace EyeTrackingResultAnalyzer.Models
{
    public class Fixation
    {
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int DurationTime { get; set; }
        public string ElementName { get; set; }

        public void Start(int index, int time)
        {
            int indexWhenFixationStarts = 0;
            if (index == indexWhenFixationStarts)
                this.StartTime = time;
        }

        public void End(string elementName, int durationTime, int endTime)
        {
            this.ElementName = elementName;
            this.DurationTime = durationTime;
            this.EndTime = endTime;
        }
    }
}
