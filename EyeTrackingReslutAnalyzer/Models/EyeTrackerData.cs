﻿namespace EyeTrackingResultAnalyzer.Models
{
    public class EyeTrackerData
    {
        public int Time { get; set; }
        public string ElementName { get; set; }
        public string DisplayName { get; set; }

        public EyeTrackerData(int time, string elementName, string displayName)
        {
            this.Time = time;
            this.ElementName = elementName;
            this.DisplayName = displayName;
        }
    }
}
