﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EyeTrackingResultAnalyzer.Models
{
    public class FixationsData
    {
        public Dictionary<string, int> TimePerElement { get; set; }
        public List<int> NumberOfTransitions { get; set; }

        public List<Fixation> Fixations { get; set; }

        //public FixationsData(Dictionary<string, int> TimePerElement, List<int> NumberOfTransitions)
        //{
        //    this.NumberOfTransitions = new List<int>();
        //    this.TimePerElement = new Dictionary<string, int>();
        //}

        public FixationsData(Dictionary<string, int> TimePerElement, List<int> NumberOfTransitions)
        {
            this.NumberOfTransitions = NumberOfTransitions;
            this.TimePerElement = TimePerElement;
        }

        public FixationsData(List<Fixation> fixations)
        {
            Fixations = fixations;
        }
    }
}
