﻿using EyeTrackingResultAnalyzer.Models;
using System.Collections.Generic;

namespace EyeTrackingResultAnalyzer.Services
{
    public class FixationCalculatorProxy : IFixationCalculatorService
    {
        public List<EyeTrackerData> GetContent(string pathToFile)
        {
            return CsvFixationCalculator.GetContent(pathToFile);
        }

        public FixationsData GetFixationsDetails(List<EyeTrackerData> fileData)
        {
            return CsvFixationCalculator.GetFixationsDetails(fileData);
        }

        public FixationsData GetTimeSpendOnModelElement(IList<EyeTrackerData> fileData)
        {
            return CsvFixationCalculator.GetTimeSpendOnModelElement(fileData);
        }
    }
}
