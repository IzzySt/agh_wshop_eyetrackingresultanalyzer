﻿using EyeTrackingResultAnalyzer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EyeTrackingResultAnalyzer.Services
{
    public class EyeTrackingAnalyzerService
    {
        private readonly IFixationCalculatorService fixationCalculatorService;

        public EyeTrackingAnalyzerService(IFixationCalculatorService fixationCalculatorService)
        {
            this.fixationCalculatorService = fixationCalculatorService;
        }

        public EyeTrackingAnalyzerService() :
            this (new FixationCalculatorProxy())
        { 

        }

        public void StartAnalyzing(string pathToFile)
        {
            bool errorOccurred = false;
            IList<EyeTrackerData> fileContent = null;

            try
            {
                fileContent = fixationCalculatorService.GetContent(pathToFile);
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine($"Error: {e.Message}");
                Console.WriteLine("Please, execute program with argument containing file path eg: EyeTrackingResultAnalyzer.exe /data/dataDemo2705.csv");
                errorOccurred = true;
            }
            if (errorOccurred) return;

            PrintFixationResults(fixationCalculatorService.GetTimeSpendOnModelElement(fileContent));
            //PrintResult(fixationCalculatorService.GetTimeSpendOnScreen(fileContent), "Display");
        }

        private void PrintFixationResults(FixationsData result)
        {
            Console.WriteLine("Test results given in milliseconds");
            Console.WriteLine("   ---   ");
            Console.WriteLine();
            Console.WriteLine("=======================Detected fixations=====================================");
            Console.WriteLine("| Start time | Duration time | End time | Focused on element  |");
            foreach (var fixation in result.Fixations)
            {
                if (fixation.ElementName == "") continue;
                Console.WriteLine($"     {fixation.StartTime}            {fixation.DurationTime}        {fixation.EndTime}           {fixation.ElementName}    ");
            }
            var summary = result.Fixations
                .GroupBy(x => x.ElementName)
                .Select(a => new { Name = a.Key, DwellTime = a.Sum(b => b.DurationTime) })
                .OrderByDescending(a => a.DwellTime)
                .ToList();

            Console.WriteLine("=================================Summary======================================");
            Console.WriteLine("| Dwell time  |  Transitions  |    Element name    |");
            foreach (var sum in summary)
            {
                if (sum.Name == "") continue;
                var transitions = result.Fixations.Where(x => x.ElementName == sum.Name).Count();
                Console.WriteLine($"     {sum.DwellTime}            {transitions}            {sum.Name}     ");
            }
        }

        private void PrintResult(FixationsData result, string title)
        {
            int counter = -1;
            foreach (var item in result.TimePerElement)
            {
                counter++;
                if (item.Key == "" || item.Key == "n/a") continue;

                Console.WriteLine("Dwell Time: " + item.Value + "     Transitions: " + "     " + title + ":    " + " - " + item.Key);
            }
        }
    }
}
