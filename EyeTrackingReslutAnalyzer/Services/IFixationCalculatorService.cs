﻿using EyeTrackingResultAnalyzer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EyeTrackingResultAnalyzer.Services
{
    public interface IFixationCalculatorService
    {
        List<EyeTrackerData> GetContent(string pathToFile);

        FixationsData GetFixationsDetails(List<EyeTrackerData> fileData);

        FixationsData GetTimeSpendOnModelElement(IList<EyeTrackerData> fileData);
    }
}
