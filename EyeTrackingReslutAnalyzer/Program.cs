﻿using EyeTrackingResultAnalyzer.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EyeTrackingResultAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            var analyzer = new EyeTrackingAnalyzerService();
            analyzer.StartAnalyzing(args[0]);
            
            Console.ReadLine();
        }

        

       
    }
}
