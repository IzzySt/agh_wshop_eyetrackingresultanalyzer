﻿using EyeTrackingResultAnalyzer.Models;
using EyeTrackingResultAnalyzer.Services;
using NSubstitute;
using System.Collections.Generic;
using Xunit;

namespace EyeTrackingResultAnalyzerTests
{
    public class FixationCalculatorTests
    {
        private readonly IFixationCalculatorService fixationCalculatorService = Substitute.For<IFixationCalculatorService>();
        private const string guestType = "Guest type";
        private const string dishes = "Dishes";
        private const string host = "Host";
        private const string dmnModel = "DMN model";
        public FixationCalculatorTests()
        {
            this.fixationCalculatorService = new FixationCalculatorProxy();
        }

        [Fact]
        public void GetFixationsDetails_ShouldReturnOneFixation_WhenTwoNextRowsDisplaysTheSame()
        {
            //Arrange                 
            const int timeA = 4855;
            const int timeB = 4889;
            const int timeC = 4895;
            List<EyeTrackerData> rows = new List<EyeTrackerData>();
            rows.Add(new EyeTrackerData(timeA, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeB, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeC, guestType, dmnModel));

            //Act
            var result = this.fixationCalculatorService.GetFixationsDetails(rows);

            //Assert
            const int durationOfFixation = timeC - timeA + 1;
            Assert.Equal(durationOfFixation, result.TimePerElement[dmnModel]);
            Assert.Single(result.TimePerElement);
        }

        [Fact]
        public void GetFixationsDetails_ShouldReturnZeroTransitions_WhenTwoNextRowsDisplaysTheSame()
        {
            //Arrange                 
            const int timeA = 4855;
            const int timeB = 4889;
            List<EyeTrackerData> rows = new List<EyeTrackerData>();
            rows.Add(new EyeTrackerData(timeA, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeB, guestType, dmnModel));

            //Act
            var result = this.fixationCalculatorService.GetFixationsDetails(rows);

            //Assert
            Assert.Equal(0, result.NumberOfTransitions[0]);
            Assert.Single(result.TimePerElement);
        }

        [Fact]
        public void GetFixationsDetails_ShouldReturnOneFixation_WhenTwoNextRowsDisplaysDiffrent()
        {
            //Arrange                 
            const int timeA = 4855;
            const int timeB = 4889;
            const int timeC = 4895;
            List<EyeTrackerData> rows = new List<EyeTrackerData>();
            rows.Add(new EyeTrackerData(timeA, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeB, guestType, host));
            rows.Add(new EyeTrackerData(timeC, guestType, dmnModel));

            //Act
            var result = this.fixationCalculatorService.GetFixationsDetails(rows);

            //Assert
            Assert.Equal(1, result.TimePerElement[dmnModel]);
            Assert.Equal(1, result.TimePerElement[host]);
        }

        [Fact]
        public void GetFixationsDetails_ShouldReturnOneTransition_WhenTwoNextRowsDisplaysDiffrent()
        {
            //Arrange                 
            const int timeA = 4855;
            const int timeB = 4889;
            const int timeC = 4895;
            List<EyeTrackerData> rows = new List<EyeTrackerData>();
            rows.Add(new EyeTrackerData(timeA, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeB, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeC, guestType, host));

            //Act
            var result = this.fixationCalculatorService.GetFixationsDetails(rows);

            //Assert
            Assert.Equal(1, result.NumberOfTransitions[0]);// 1 transition for dnmModel el
            Assert.Equal(0, result.NumberOfTransitions[1]); // 0 transition for host el
        }

        [Fact]
        public void GetTimeSpendOnModelElement_ShouldReturn0Fixation_WhenTimeDiffLessThan150ms()
        {
            //Arrange
            //timeC - TimeA < 150ms
            const int timeA = 4000;
            const int timeB = 4149;
            const int timeC = 4895; 
            List<EyeTrackerData> rows = new List<EyeTrackerData>();
            rows.Add(new EyeTrackerData(timeA, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeB, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeC, dishes, dmnModel));

            //Act
            var result = this.fixationCalculatorService.GetTimeSpendOnModelElement(rows);

            //Assert
            Assert.Empty(result.Fixations);
        }

        [Fact]
        public void GetTimeSpendOnModelElement_ShouldReturn0Fixation_WhenGazeCoordinationNotChanged()
        {
            const int timeA = 4855;
            const int timeB = 4889;
            const int timeC = 4995;
            List<EyeTrackerData> rows = new List<EyeTrackerData>();
            rows.Add(new EyeTrackerData(timeA, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeB, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeC, guestType, dmnModel));

            //Act
            var result = this.fixationCalculatorService.GetTimeSpendOnModelElement(rows);

            //Assert
            Assert.Empty(result.Fixations);
        }

        [Fact]
        public void GetTimeSpendOnModelElement_ShouldReturn1Fixation_WhenFixationLongerThan150ms()
        {
            const int timeA = 4000;
            const int timeB = 4999;
            const int timeC = 5000;
            List<EyeTrackerData> rows = new List<EyeTrackerData>();
            rows.Add(new EyeTrackerData(timeA, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeB, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeC, dishes, dmnModel));

            //Act
            var result = this.fixationCalculatorService.GetTimeSpendOnModelElement(rows);

            //Assert
            Assert.Single(result.Fixations);
        }

        [Fact]
        public void GetTimeSpendOnModelElement_ShouldReturn1Fixation_WhenFixationEquals150ms()
        {
            const int timeA = 4000;
            const int timeB = 4150;
            const int timeC = 5000;
            List<EyeTrackerData> rows = new List<EyeTrackerData>();
            rows.Add(new EyeTrackerData(timeA, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeB, guestType, dmnModel));
            rows.Add(new EyeTrackerData(timeC, dishes, dmnModel));

            //Act
            var result = this.fixationCalculatorService.GetTimeSpendOnModelElement(rows);

            //Assert
            Assert.Single(result.Fixations);
        }
    }

}
